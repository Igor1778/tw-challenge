from process_utilities import ProcessUtilities
from hotel import Hotel


class HotelUtilities(object):
    def get_right_hotels(self, entries):
        result = []

        for entry in entries:
            hotel_for_entry = self.get_right_hotel_for_entry(self, entry)
            result.append(hotel_for_entry)

        return result

    def get_right_hotel_for_entry(self, entry):
        week_count = ProcessUtilities.process_days_of_the_week(
            ProcessUtilities, entry, False)
        weekend_count = ProcessUtilities.process_days_of_the_week(
            ProcessUtilities, entry, True)
        is_reward = ProcessUtilities.process_fidelity(entry)

        hotels = Hotel.initialize_hotels()
        hotel_totals = []
        min_total = float('inf')

        for hotel in hotels:
            hotel_total = hotel.calculate_amount_spent(
                week_count, weekend_count, is_reward)
            min_total = min(min_total, hotel_total)
            hotel_totals.append(hotel_total)

        result = self.get_cheapest_or_highest_classification_hotel(
            hotels, hotel_totals, min_total)

        return result

    @staticmethod
    def get_cheapest_or_highest_classification_hotel(hotels, hotel_totals, min_total):
        ties = []
        for i in range(len(hotel_totals)):
            if hotel_totals[i] == min_total:
                ties.append(hotels[i])

        ties.sort(key=lambda x: x.classification, reverse=True)

        if len(ties) > 1:
            return ties[0].name
        else:
            for i in range(len(hotel_totals)):
                if hotel_totals[i] == min_total:
                    return hotels[i].name

import os.path


class IOUtilities(object):
    def get_valid_data(self, file_path):
        if not os.path.isfile(file_path):
            raise Exception('File not found')

        data = []
        file_obj = open(file_path)

        for entry in file_obj.readlines():
            data.append(entry.rstrip())

        for entry in data:
            self.validate_entry(entry)

        file_obj.close()
        return data

    @staticmethod
    def validate_entry(entry):
        if ':' not in entry:
            raise Exception('Invalid schedule format')

        fidelity_plan = entry.split(':')[0]
        if fidelity_plan != 'Rewards' and fidelity_plan != 'Regular':
            raise Exception('Invalid fidelity plan')

        schedule = entry.split(':')[1]
        dates = [d.strip() for d in schedule.split(',')]
        days = ['mon', 'tues', 'wed', 'thur', 'fri', 'sat', 'sun']

        for date in dates:
            if '(' not in date or ')' not in date:
                raise Exception('Invalid date format')

            cur_day = date[date.find("(")+1:date.find(")")]
            if cur_day not in days:
                raise Exception('Invalid date format')

        return entry

    @staticmethod
    def print_instructions_and_get_file_path():
        print("Welcome to Miami Hotels! Please follow the instructions so that we "
              "can find the best hotel for you.\n")

        print("Step 1: Create a file with the entries (fidelity plan and schedule). "
              "This file should have only one entry per line and they should be in the following "
              "format: \"Regular: 20Mar2009(fri), 21Mar2009(sat), 22Mar2009(sun)\"\n")

        print("Step 2: Enter the location path of the file with the entries on the console and press enter. "
              "Supported formats: .txt and .md files. E.g. \"test_input.txt\"\n")

        file_path = input("Enter file path: ")

        return file_path

    @staticmethod
    def output_result(entries, results):
        print("\nResults:\n")

        for i in range(len(entries)):
            print("For the schedule: \"" +
                  entries[i] + "\", the best hotel is: " + results[i])

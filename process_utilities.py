class ProcessUtilities(object):
    def process_days_of_the_week(self, entry, is_weekend):
        dates = self.process_dates(entry)
        week_days = ['mon', 'tues', 'wed', 'thur', 'fri']
        weekend_days = ['sat', 'sun']
        days_count = 0

        for date in dates:
            cur_day = date[date.find("(")+1:date.find(")")]

            if is_weekend and cur_day in weekend_days:
                days_count += 1

            if not is_weekend and cur_day in week_days:
                days_count += 1

        return days_count

    @staticmethod
    def process_dates(valid_input):
        schedule = valid_input.split(':')[1]
        dates = [d.strip() for d in schedule.split(',')]
        return dates

    @staticmethod
    def process_fidelity(valid_input):
        fidelity_plan = valid_input.split(':')[0]
        is_reward = True if fidelity_plan == 'Rewards' else False
        return is_reward

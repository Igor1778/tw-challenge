from hotel_utilities import HotelUtilities
from io_utilities import IOUtilities
from process_utilities import ProcessUtilities

def main():
    file_path = IOUtilities.print_instructions_and_get_file_path()
    valid_data = IOUtilities.get_valid_data(IOUtilities, file_path)
    results = HotelUtilities.get_right_hotels(HotelUtilities, valid_data)
    IOUtilities.output_result(valid_data, results)


if __name__ == '__main__':
    main()

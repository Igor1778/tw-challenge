# Miami Hotel Challenge

## Introduction

This application is supposed to find the cheapest hotel from Miami Hotels chain
company. To do so, a few assumptions were made:

- The chain is composed by only Lakewood, Bridgewood and Ridgewood hotels, with
  constant classifications and prices for week and weekend days (depending on
  the fidelity plan). More hotels can easily be added, and classification and
  prices can be changed by modifying `hotels_table.txt` file.
- There are only two fidelity plans for all hotels, `Regular` and `Reward`.
- The day of the week will always be specified between parentheses for the dates
  provided. E.g. `16Mar2009(mon)` and `16/03/09(mon)` are valid dates, but
  `16Mar2009` is not.
- Users will understand how to create a file with entries (fidelity plan and
  schedule) and enter its location path to the console by following the
  instructions given.

## How to run the project

Make sure you have Python3 installed in your machine. It was developed using
Python 3.7.3 but any version 3.x.x should work.

### Using the app

- Clone or download the project and `cd` into the folder.
- Run the `python main.py` command.
- Follow the instructions printed in the terminal.

### Running the tests

- Clone or download the project and `cd` into the folder.
- Run the `python test.py` command.

## Additional Information

If you have any trouble running the project or the tests, please send a message
to `igor.wilbert@gmail.com`.

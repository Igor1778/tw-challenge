class Hotel(object):
    def __init__(self, name, classification, week_regular, week_reward, weekend_regular, weekend_reward):
        self.name = name
        self.classification = classification
        self.week_regular = week_regular
        self.week_reward = week_reward
        self.weekend_regular = weekend_regular
        self.weekend_reward = weekend_reward

    def calculate_amount_spent(self, week_days, weekend_days, is_reward):
        week_daily_fee = self.week_reward if is_reward else self.week_regular
        weekend_daily_fee = self.weekend_reward if is_reward else self.weekend_regular

        week_total = week_days * week_daily_fee
        weekend_total = weekend_days * weekend_daily_fee

        return week_total + weekend_total

    @staticmethod
    def initialize_hotels():
        hotels = []
        hotels_data = []
        file_obj = open('hotels_table.txt')

        for entry in file_obj.readlines()[1:]:
            hotels_data.append(entry.rstrip())

        for entry in hotels_data:
            [hotel_name, classification, week_day_price_regular, week_day_price_reward,
                weekend_day_price_regular, weekend_day_price_reward] = entry.split(',')

            hotel = Hotel(hotel_name, int(classification), int(week_day_price_regular), int(
                week_day_price_reward), int(weekend_day_price_regular), int(weekend_day_price_reward))

            hotels.append(hotel)

        return hotels

import unittest
from hotel import Hotel
from hotel_utilities import HotelUtilities
from io_utilities import IOUtilities
from process_utilities import ProcessUtilities


class TestStringMethods(unittest.TestCase):

    # should return cheapest hotel if there is no price tie
    def test_get_cheapest_or_highest_classification_hotel_1(self):
        lakewood_hotel = Hotel("Lakewood", 3, 110, 80, 90, 80)
        bridgewood_hotel = Hotel("Bridgewood", 4, 160, 110, 60, 50)
        ridgewood_hotel = Hotel("Ridgewood", 5, 220, 100, 150, 40)

        hotels = [lakewood_hotel, bridgewood_hotel, ridgewood_hotel]
        hotel_totals = [330, 480, 660]  # ith total is from ith hotel
        min_total = 330

        self.assertEqual(HotelUtilities.get_cheapest_or_highest_classification_hotel(
            hotels, hotel_totals, min_total), 'Lakewood')

    # should return highest classification hotel if there is a price tie
    def test_get_cheapest_or_highest_classification_hotel_2(self):
        lakewood_hotel = Hotel("Lakewood", 3, 110, 80, 90, 80)
        bridgewood_hotel = Hotel("Bridgewood", 4, 160, 110, 60, 50)
        ridgewood_hotel = Hotel("Ridgewood", 5, 220, 100, 150, 40)

        hotels = [lakewood_hotel, bridgewood_hotel, ridgewood_hotel]
        hotel_totals = [240, 270, 240]  # ith total is from ith hotel
        min_total = 240

        self.assertEqual(HotelUtilities.get_cheapest_or_highest_classification_hotel(
            hotels, hotel_totals, min_total), 'Ridgewood')

    # should return the right day counts for days of the week and weekend
    def test_process_days_of_the_week(self):
        entry = "Regular: 16Mar2009(sun), 17Mar2009(mon), 18Mar2009(tues)"

        week_count = ProcessUtilities.process_days_of_the_week(
            ProcessUtilities, entry, False)
        weekend_count = ProcessUtilities.process_days_of_the_week(
            ProcessUtilities, entry, True)

        self.assertEqual(week_count, 2)
        self.assertEqual(weekend_count, 1)

    # should raise exception if entry format is invalid
    def test_validate_entry(self):
        invalid_schedule_format = "Regular - 16Mar2009(mon), 17Mar2009(tu), 18Mar2009(wed)"
        invalid_date_format = "Regular: 16Mar2009(mon), 17Mar2009(tu), 18Mar2009(wed)"
        invalid_fidelity_plan = "Premium: 16Mar2009(mon), 17Mar2009(tues), 18Mar2009(wed)"

        with self.assertRaises(Exception) as invalid_schedule_format_context:
            IOUtilities.validate_entry(invalid_schedule_format)

        with self.assertRaises(Exception) as invalid_date_format_context:
            IOUtilities.validate_entry(invalid_date_format)

        with self.assertRaises(Exception) as invalid_fidelity_plan_context:
            IOUtilities.validate_entry(invalid_fidelity_plan)

        self.assertTrue('Invalid schedule format' in str(
            invalid_schedule_format_context.exception))
        self.assertTrue('Invalid date format' in str(
            invalid_date_format_context.exception))
        self.assertTrue('Invalid fidelity plan' in str(
            invalid_fidelity_plan_context.exception))

    # should raise exception if file does not exist
    def test_get_valid_data_1(self):
        with self.assertRaises(Exception) as file_not_found_context:
            IOUtilities.get_valid_data(IOUtilities, "")

        self.assertTrue('File not found' in str(
            file_not_found_context.exception))

    # should return valid data if the file exists and has valid data
    def test_get_valid_data_2(self):
        valid_data = ["Regular: 16Mar2009(mon), 17Mar2009(tues), 18Mar2009(wed)",
                      "Regular: 20Mar2009(fri), 21Mar2009(sat), 22Mar2009(sun)",
                      "Rewards: 26Mar2009(thur), 27Mar2009(fri), 28Mar2009(sat)"]

        returned_data = IOUtilities.get_valid_data(
            IOUtilities, "test_input.txt")

        self.assertEqual(valid_data, returned_data)


if __name__ == '__main__':
    unittest.main()
